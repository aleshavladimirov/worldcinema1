//
//  Films.swift
//  WorldCinema2
//
//  Created by Преподаватель on 16.11.2021.
//

import Foundation

class Films{
    
    
    enum collectionFilms: String{
        case new = "new"
        case inTrend = "InTrend"
        case forMe = "forMe"
    }
    func getFilmsForCollection(filter: collectionFilms) -> [FilmsModel]? {
        var films: [FilmsModel]?
        let  semaphore = DispatchSemaphore (value: 0)

        var request = URLRequest(url: URL(string: "http://cinema.areas.su/movies?filter=\(filter)")!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            semaphore.signal()
            return
          }
            films = try? JSONDecoder().decode([FilmsModel].self, from: data)
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()

        return films
    }
}
