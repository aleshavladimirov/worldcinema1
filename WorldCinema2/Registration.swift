//
//  Registration.swift
//  WorldCinema2
//
//  Created by Преподаватель on 15.11.2021.
//

import Foundation

class Registration{
    func Reg(email: String,password: String, firstName: String, lastName: String) -> Bool?{
        let semaphore = DispatchSemaphore (value: 0)
        var result = ""
        
        let parameters = "{\n  \"email\": \"\(email)\",\n  \"password\": \"\(password)\",\n  \"firstName\": \"\(firstName)\",\n  \"lastName\": \"\(lastName)\"\n}"
        let postData = parameters.data(using: .utf8)

        var request = URLRequest(url: URL(string: "http://cinema.areas.su/auth/register")!,timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            semaphore.signal()
            return
          }
            result = String (data: data, encoding: .utf8)!
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        if result == "Успешная регистрация"{
            return true
        }
        return false

    }
}
