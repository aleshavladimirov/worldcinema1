//
//  SignUpScreen.swift
//  WorldCinema2
//
//  Created by Преподаватель on 15.11.2021.
//

import UIKit

class SignUpScreen: UIViewController {

    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CornerFunc()
    }
    
    @IBAction func registrationCheck(_ sender: Any) {
        if !email.hasText || email.text == "E-mail"{
            let alert = UIAlertController(title: "Ошибка ввода", message: "Введите email", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        if !password.hasText || email.text == "Пароль"{
            let alert = UIAlertController(title: "Ошибка ввода", message: "Введите пароль", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
            let email1 = email.text!
            let emailStr = email1.split(separator: "@")
            
            if emailStr.count != 2{
                let alert = UIAlertController(title: "Ошибка ввода", message: "Неправильный формат E-mail", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
                present(alert, animated: true,completion: nil)
                return
            }
        
        let authorization = Authorization().Auth(email: email.text!, password: password.text!)
        guard authorization == "" else{
            let alert = UIAlertController(title: "Ошибка ввода", message: "Аккаунта с таким логином и паролем не существует", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        let vc = storyboard?.instantiateViewController(withIdentifier: "main")
        present (vc!, animated: true, completion: nil)
        }
    
    
    
    
    
    func CornerFunc(){
        email.layer.cornerRadius = 5
        email.layer.borderWidth = 1
        email.clipsToBounds = true
        email.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor
        password.layer.cornerRadius = 5
        password.layer.borderWidth = 1
        password.clipsToBounds = true
        password.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor
        signUpButton.layer.cornerRadius = 5
        signUpButton.layer.borderWidth = 1
        signUpButton.clipsToBounds = true
        signUpButton.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor
        registrationButton.layer.cornerRadius = 5
        registrationButton.layer.borderWidth = 1
        registrationButton.clipsToBounds = true
        registrationButton.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor
        
    }
}
