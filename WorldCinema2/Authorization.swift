//
//  File.swift
//  WorldCinema2
//
//  Created by Преподаватель on 15.11.2021.
//

import Foundation

class Authorization{
    struct AuthorizationModel: Decodable{
        let token: Int
    }
    func Auth(email: String, password: String) -> String{
        var result = ""
        let semaphore = DispatchSemaphore (value: 0)

        let parameters = "{\n  \"email\": \"\(email)\",\n  \"password\": \"\(password)\"\n}"
        let postData = parameters.data(using: .utf8)

        var request = URLRequest(url: URL(string: "http://cinema.areas.su/auth/login")!,timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
            let authorizationModel = try? JSONDecoder().decode(AuthorizationModel.self, from: data)
            if authorizationModel == nil{
                result = String(data: data, encoding: .utf8)!
            }
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        return result
    }
}


