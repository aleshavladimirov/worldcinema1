//
//  File.swift
//  WorldCinema2
//
//  Created by Преподаватель on 16.11.2021.
//

import Foundation


class Cover{

    
    func getCover() -> CoverModel?{
        var returnData: CoverModel?
    let semaphore = DispatchSemaphore (value: 0)

    var request = URLRequest(url: URL(string: "http://cinema.areas.su/movies/cover")!,timeoutInterval: Double.infinity)
    request.httpMethod = "GET"

    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      guard let data = data else {
        print(String(describing: error))
        semaphore.signal()
        return
      }
        returnData = try? JSONDecoder().decode(CoverModel.self, from: data)
      semaphore.signal()
    }

    task.resume()
    semaphore.wait()
        return returnData
}
}
