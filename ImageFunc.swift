//
//  ImageFunc.swift
//  WorldCinema2
//
//  Created by Преподаватель on 16.11.2021.
//

import Foundation
import UIKit

extension UIImage{
    
    static func getImageFromUrl(startHttp: Bool = true, url: String) -> UIImage{
        if startHttp{
            let image = try? Data(contentsOf: URL(string: "http://cinema.areas.su/up/images/" + url)!)
            if image == nil{
                return UIImage()
            }
        return UIImage(data: image!)!
        }
        
        
        let data = try? Data(contentsOf: URL(string: url)!)
        if data == nil{
            return UIImage()
        }
        return UIImage(data: data!)!
    }
}

