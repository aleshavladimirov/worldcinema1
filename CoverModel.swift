//
//  CoverModel.swift
//  WorldCinema2
//
//  Created by Преподаватель on 16.11.2021.
//

import Foundation

struct CoverModel: Decodable{
    let movieId: String
    let backgroundImage: String
    let foregroundImage: String
}
