//
//  FilmsModel.swift
//  WorldCinema2
//
//  Created by Преподаватель on 16.11.2021.
//

import Foundation

struct FilmsModel: Decodable{
    let movieId: String
    let name: String
    let description: String
    let age: String
    let images: [String]?
    let poster: String
    let tags: [Tags]
}

struct Tags: Decodable{
    let idTags: String
    let tagName: String
}
