//
//  MainScreen.swift
//  WorldCinema2
//
//  Created by Преподаватель on 16.11.2021.
//

import UIKit

class MainScreen: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate{
    
    
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var inTrendCollectionView: UICollectionView!
    @IBOutlet weak var newCollectionView: UICollectionView!
    @IBOutlet weak var forMeCollectionView: UICollectionView!
    
    
    let inTrendFilms = Films().getFilmsForCollection(filter: .inTrend)
    let forMeFilms = Films().getFilmsForCollection(filter: .forMe)
    let newFilms = Films().getFilmsForCollection(filter: .new)
    override func viewDidLoad() {
        super.viewDidLoad()
        posterImage.image = UIImage.getImageFromUrl(url: "magicians.png")

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == inTrendCollectionView{
            return inTrendFilms?.count ?? 0
        }
        if collectionView == newCollectionView{
            return newFilms?.count ?? 0
        }
        if collectionView == forMeCollectionView{
            return forMeFilms?.count ?? 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == inTrendCollectionView{
            let inTrendCell = collectionView.dequeueReusableCell(withReuseIdentifier: "inTrendCell", for: indexPath) as! InTrendCollectionViewCell
            inTrendCell.InTrendPoster.image = UIImage.getImageFromUrl(url: inTrendFilms![indexPath.item].poster)
            return inTrendCell
        }
            if collectionView == newCollectionView{
                let newCell = collectionView.dequeueReusableCell(withReuseIdentifier: "newCell", for: indexPath) as! NewCollectionViewCell
                newCell.newPoster.image = UIImage.getImageFromUrl(url: newFilms![indexPath.item].poster)
                return newCell
            }
        if collectionView == forMeCollectionView{
            let forMeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "forMeCell", for: indexPath) as! ForMeCollectionViewCell
            forMeCell.forMePoster.image = UIImage.getImageFromUrl(url: forMeFilms![indexPath.item].poster)
            return forMeCell
        }
        return UICollectionViewCell()
        }
    

}
