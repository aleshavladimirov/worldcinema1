//
//  SignInScreen.swift
//  WorldCinema2
//
//  Created by Преподаватель on 16.11.2021.
//

import UIKit

class SignInScreen: UIViewController {
    @IBOutlet weak var firstNameFIeld: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var email1: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var repeatpasswordField: UITextField!
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CornerFunc()
    }
    
    @IBAction func registryCheck(_ sender: Any) {
        if(!email1.hasText || email1.text == "E-mail") {
            let alert = UIAlertController(title: "Ошибка ввода", message: "Введен некорректный E-mail", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
            if(!firstNameFIeld.hasText || firstNameFIeld.text == "Имя") {
                let alert = UIAlertController(title: "Ошибка ввода", message: "Введено некорректное Имя", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
                present(alert, animated: true, completion: nil)
                return
            }
            if(!email1.hasText || email1.text == "Фамилия") {
                let alert = UIAlertController(title: "Ошибка ввода", message: "Введена некорректная Фамилия", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
                present(alert, animated: true, completion: nil)
                return
            }
            if(!passwordField.hasText || passwordField.text == "Пароль") {
                let alert = UIAlertController(title: "Ошибка ввода", message: "Введен некорректный Пароль", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
                present(alert, animated: true, completion: nil)
                return
            }
            if passwordField.text != repeatpasswordField.text{
                let alert = UIAlertController(title: "Ошибка ввода", message: "Пароли не совпадают", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
                present(alert, animated: true, completion: nil)
                return
            }
            if(!repeatpasswordField.hasText || repeatpasswordField.text == "Повторите пароль") {
                let alert = UIAlertController(title: "Ошибка ввода", message: "Введен некорректный пароль", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
                present(alert, animated: true, completion: nil)
                return
            }
            let email = email1.text!
            let emailStr = email.split(separator: "@")
            if emailStr.count != 2 {
                let alert = UIAlertController(title: "Ошибка", message: "Нерпавильный формат E-mail" , preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
                present(alert, animated: true, completion: nil)
                return
            }
            let emailStr1 = emailStr[1].split(separator: ".")
            if emailStr1.count < 2 || emailStr1.last!.count > 3 {
                let alert = UIAlertController(title: "Ошибка", message: "Неправильный формат E-mail", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
                present(alert,animated: true, completion: nil)
                return
            }
        guard Registration().Reg(email: email1.text!, password: passwordField.text!, firstName: firstNameFIeld.text!, lastName: lastNameField.text!) ?? false else{
            let alert = UIAlertController(title: "Ошибка", message: "Ошибка регистрации", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        let vc = storyboard?.instantiateViewController(withIdentifier: "main")
        present (vc!, animated: true, completion: nil)
    }
    
            
    
    
    func CornerFunc(){
        email1.layer.cornerRadius = 5
        email1.layer.borderWidth = 1
        email1.clipsToBounds = true
        email1.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor
        passwordField.layer.cornerRadius = 5
        passwordField.layer.borderWidth = 1
        passwordField.clipsToBounds = true
        passwordField.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor
        signUpButton.layer.cornerRadius = 5
        signUpButton.layer.borderWidth = 1
        signUpButton.clipsToBounds = true
        signUpButton.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor
        registrationButton.layer.cornerRadius = 5
        registrationButton.layer.borderWidth = 1
        registrationButton.clipsToBounds = true
        registrationButton.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor
        
    }
}
    
